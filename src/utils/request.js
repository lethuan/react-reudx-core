import fetch from 'dva/fetch';
import { request, GraphQLClient } from 'graphql-request'

function parseJSON(response) {
  return response.json();
}

function checkStatus(response) {
  if (response) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export const requestGraphQL = (options = {}) => {
  const client = new GraphQLClient('https://dev-doc-api.zody.vn/graphql', { method: options.method, headers: {} })
  return client.request(options.query, options.variables)
    .then(data => ({ data }))
    .catch(err => ({ err }));
}
