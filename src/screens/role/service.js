import { requestGraphQL } from '../../utils'
import { ApiConst } from '../../configs'

export async function all(data) {
  const api = ApiConst.role.all()
  return requestGraphQL({
    method: api.method,
    query: api.query,
    variables: data.variables
  })
}

export async function show(data) {
  const api = ApiConst.role.show()
  return requestGraphQL({
    method: api.method,
    query: api.query,
    variables: data.variables
  })
}

export async function create(data) {
  const api = ApiConst.role.create()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

export async function update(data) {
  const api = ApiConst.role.update()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

export async function destroy(data) {
  const api = ApiConst.role.destroy()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data
  })
}

export async function changeStatus(data) {
  const api = ApiConst.role.changeStatus()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

export async function allPermission(data = {}) {
  const api = ApiConst.permission.all()
  return requestGraphQL({
    method: api.method,
    query: api.query,
    variables: data.variables
  })
}
