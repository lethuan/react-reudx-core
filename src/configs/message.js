export default {
  ServerError: 'Đã có lỗi xảy ra, vui lòng thử lại!',
  NoPermission: 'Bạn không có quyền thực hiện hành động này, vui lòng liên hệ với Zody để biết thêm chi tiết!',

  // Auth
  Login: {
    RequireEmailAndPassword: 'Vui lòng điền đầy đủ thông tin đăng nhập',
    EmailIsNotValid: 'Email không đúng định dạng',
    PasswordLengthMustAtLeast6Characters: 'Mật khẩu phải có ít nhất 6 ký tự',
    Successfully: 'Đăng nhập thành công',
    UserBanned: 'Tài khoản của bạn đã bị khoá, vui lòng liên hệ với Zody để biết thêm chi tiết!'
  }
}
