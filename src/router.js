import React from 'react'
import dynamic from 'dva/dynamic'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect, routerRedux } from 'dva/router'
import viVn from 'antd/lib/locale-provider/vi_VN'
import { LocaleProvider } from 'antd'
import { LayoutView } from './screens/layout'
import { AllUserView, UserModel, EditUserView } from './screens/user'
import { AllRoleView, RoleModel, EditRoleView } from './screens/role'

const { ConnectedRouter } = routerRedux

function Routers({ history, app }) {
  // Routes
  const routes = [{
    path: '/user',
    models: () => [UserModel],
    component: () => AllUserView
  }, {
    path: '/user/create',
    models: () => [UserModel],
    component: () => EditUserView
  }, {
    path: '/user/:id/edit',
    models: () => [UserModel],
    component: () => EditUserView
  }, {
    path: '/role',
    models: () => [RoleModel],
    component: () => AllRoleView
  }, {
    path: '/role/create',
    models: () => [RoleModel],
    component: () => EditRoleView
  }, {
    path: '/role/:id/edit',
    models: () => [RoleModel],
    component: () => EditRoleView
  }]

  return (
    <LocaleProvider locale={viVn}>
      <ConnectedRouter history={history}>
        <LayoutView>
          <Switch>
            {
              routes.map(({ path, ...dynamics }, key) => (
                <Route
                  key={key}
                  exact
                  path={path}
                  component={dynamic({
                    app,
                    ...dynamics
                  })}
                />
              ))
            }
          </Switch>
        </LayoutView>
      </ConnectedRouter>
    </LocaleProvider>
  )
}

Routers.propTypes = {
  history: PropTypes.object,
  app: PropTypes.object
}

export default Routers
