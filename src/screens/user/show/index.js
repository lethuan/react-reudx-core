import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Modal, Row, Col, Avatar } from 'antd'
import './style.less'

class ShowUser extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    id: PropTypes.string,
    visible: PropTypes.bool,
    onClose: PropTypes.func
  }

  constructor(props) {
    super(props)
    this.state = {
      avatar: 'https://zodyapp-dev.s3.amazonaws.com/sm_766556415877_1513221612568.png'
    }
    this.isLoaded = false
  }

  componentWillReceiveProps(newProps) {
    const { visible, id } = newProps
    if (visible && !this.isLoaded) {
      this.isLoaded = true
      this.showUser(id)
    }
  }

  showUser = (id) => {
    this.props.dispatch({
      type: 'user/show',
      payload: {
        variables: {
          userID: id
        }
      }
    })
  }
  toggle = () => {
    this.isLoaded = false
    this.props.onClose()
  }

  render() {
    const { user: { show } } = this.props
    return (
      <Modal
        title="THÔNG TIN THÀNH VIÊN"
        style={{ top: 40 }}
        width="40%"
        visible={this.props.visible}
        onOk={this.toggle}
        onCancel={this.toggle}
        afterClose={this.toggle}
        footer={null}
        className="modal-customer-data"
      >
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Row>
              <Col md={24} lg={12} xl={8} xs={24} sm={24}>
                <Avatar src={this.state.avatar} className="profile-avatar" shape="square" />
              </Col>
              <Col md={24} lg={12} xl={12} xs={24} sm={24}>
                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>Tên</td>
                      <td className="table-data">{show.name}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td className="table-data">{show.email}</td>
                    </tr>
                    <tr>
                      <td>Role</td>
                      <td className="table-data">{show.role}</td>
                    </tr>
                  </tbody>
                </table>
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal>
    )
  }
}

export default connect(({ user }) => ({ user }))(ShowUser)
