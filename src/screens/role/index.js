import AllRoleView from './view'
import RoleModel from './model'
import EditRoleView from './edit/view'

export {
  AllRoleView,
  RoleModel,
  EditRoleView
}
