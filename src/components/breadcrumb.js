import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Icon } from 'antd'
import './style.less'

class RcBreadcrumb extends React.Component {
  static propTypes = {
    name: PropTypes.string
  }

  render() {
    return (
      <Breadcrumb className="rc-breadcrumb">
        <Breadcrumb.Item><Icon type="shop" />&nbsp;&nbsp;Cửa hàng</Breadcrumb.Item>
        <Breadcrumb.Item>{this.props.name}</Breadcrumb.Item>
      </Breadcrumb>
    )
  }
}

export default RcBreadcrumb
