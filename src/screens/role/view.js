import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Link } from 'react-router-dom'
import { Row, Layout, Button } from 'antd'
import { RcBreadcrumb } from '../../components'
import TableView from './table/index'

class ViewRole extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    loading: PropTypes.object
  }

  componentWillMount() {
    const { role: { isLoaded } } = this.props
    if (isLoaded) {
      return
    }
    this.getList()
  }

  getList = () => {
    this.props.dispatch({
      type: 'role/all',
      payload: {
        variables: { page: 0 }
      }
    })
  }
  deleteRole = (roleID, index) => {
    this.props.dispatch({
      type: 'role/destroy',
      payload: {
        index,
        variables: { roleID }
      }
    })
  }

  render() {
    const { role: { all } } = this.props
    return (
      <Layout className="container">
        <Row type="flex" justify="space-between">
          <RcBreadcrumb name="Role" />
          <Link to={'/role/create'}>
            <Button type="primary" className="customer-button-create" size={'large'}>Tạo thành viên</Button>
          </Link>
        </Row>
        <Layout className="page-content">
          <Row>
            <div className="section-title">
              <h4>Danh sách role</h4>
            </div>
            <TableView onDelete={this.deleteRole} data={all} pageSize={all.length} current={0} total={all.length} />
          </Row>
        </Layout>
      </Layout>

    )
  }
}

export default connect(({ role, loading }) => ({ role, loading }))(ViewRole)
