import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Layout, Row, Button } from 'antd'
import { Link } from 'react-router-dom'
import { RcBreadcrumb } from '../../components'
import TableView from './table'
import ShowUser from './show/index'

class UserView extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    loading: PropTypes.object
  }
  constructor(props) {
    super(props)

    this.state = {
      id: '',
      modalCustomerInfoVisible: false
    }
  }

  componentWillMount() {
    const { user: { isLoaded } } = this.props
    if (isLoaded) {
      return
    }
    this.getListUser()
  }
  // View user detail
  onViewUser = (id) => {
    this.setState({
      id,
      modalCustomerInfoVisible: true
    })
  }
  // Close customer modal
  onCloseModalCustomerInfo = () => {
    this.setState({
      id: '',
      modalCustomerInfoVisible: false
    })
  }

  getListUser = () => {
    this.props.dispatch({
      type: 'user/listUser',
      payload: {
        variables: { page: 0 }
      }
    })
  }


  render() {
    const { user: { data } } = this.props
    return (
      <Layout className="container">
        <Row type="flex" justify="space-between">
          <RcBreadcrumb name="Thành viên" />
          <Link to={'/user/create'}>
            <Button type="primary" className="customer-button-create" size={'large'}>Tạo thành viên</Button>
          </Link>
        </Row>
        <Layout className="page-content">
          <Row>
            <div className="section-title">
              <h4>Danh sách thành viên</h4>
            </div>
            <TableView onViewUser={this.onViewUser} data={data} pageSize={data.length} current={0} total={data.length} />
          </Row>
          <ShowUser
            visible={this.state.modalCustomerInfoVisible}
            id={this.state.id}
            onClose={this.onCloseModalCustomerInfo}
            dispatch={this.props.dispatch}
          />
        </Layout>
      </Layout>
    )
  }

}

export default connect(({ user, loading }) => ({ user, loading }))(UserView)
