import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Link } from 'react-router-dom'
import { Layout, Row, Col, Button, Select, Avatar, Form, Icon, Input, Upload } from 'antd'
import { RcBreadcrumbs } from '../../../components'

const FormItem = Form.Item
const Option = Select.Option
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  }
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 6,
    },
  }
}
const updateBreadcrumbs = [{ isLink: true, name: 'Thành viên', path: '/user' }, {
  isLink: false,
  name: 'Chỉnh sửa',
  path: ''
}]
const createBreadcrumbs = [{ isLink: true, name: 'Thành viên', path: '/user' }, {
  isLink: false,
  name: 'Tạo',
  path: ''
}]
const phoneValidate = /^0(1\d{9}|9\d{8})$/
const roles = [{
  id: 'manager',
  text: 'Manager'
}, {
  id: 'developer',
  text: 'Developer'
}, {
  id: 'tester',
  text: 'Tester'
}]

class FormUser extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    loading: PropTypes.object
  }

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      phone: '',
      role: '',
      password: '',
      avatar: 'https://zodyapp-dev.s3.amazonaws.com/sm_766556415877_1513221612568.png',
      file: ''
    }
  }

  componentWillMount() {
    this.editOrCreate()
  }

  componentWillReceiveProps(nextProps) {
    this.resetState(nextProps)
  }

  onChangeName = (name) => {
    this.setState({
      name
    })
  }
  onChangeEmail = (email) => {
    this.setState({
      email
    })
  }
  onChanePassword = (password) => {
    this.setState({
      password
    })
  }
  onChangePhone = (phone) => {
    this.setState({
      phone
    })
  }
  onChangeRole = (role) => {
    this.setState({
      role
    })
  }
  onCreateUser = () => {
    const { email, name, password, role } = this.state
    this.props.dispatch({
      type: 'user/create',
      payload: {
        variables: {
          userCreate: {
            name,
            email,
            password,
            role
          }
        }
      }
    })
  }
  onUpdateUser = () => {
    const { name, phone, role } = this.state
    this.props.dispatch({
      type: 'user/update',
      payload: {
        variables: {
          userID: this.props.match.params.id,
          userCreate: {
            name,
            phone,
            role
          }
        }
      }
    })
  }
  uploadImage = (file) => {
    console.log(file)
    // UploadFileMutation(file, response => console.log(response))
  }
  changeStatus = () => {
    this.props.dispatch({
      type: 'user/changeStatus',
      payload: {
        variables: {
          userID: this.props.match.params.id,
        }
      }
    })
  }
  showUser = () => {
    this.props.dispatch({
      type: 'user/show',
      payload: {
        variables: {
          userID: this.props.match.params.id
        }
      }
    })
  }
  resetState = (props) => {
    const { user: { show } } = props
    this.setState({
      name: show.name,
      email: show.email,
      role: show.role,
      phone: show.phone,
      active: show.active
    })
  }
  editOrCreate = () => {
    if (this.props.match.params.id) {
      this.showUser()
    } else {
      this.setState({
        name: '',
        email: '',
        phone: '',
        role: '',
        password: '',
      })
    }
  }

  render() {
    const { id } = this.props.match.params
    const { getFieldDecorator } = this.props.form
    return (
      <Layout className="container">
        <RcBreadcrumbs
          name={id ? updateBreadcrumbs : createBreadcrumbs}
        />
        <Layout className="page-content">
          <Row>
            <div className="customer-box">
              <Col md={12} lg={12} sm={24} xs={24}>
                <Form>
                  <FormItem {...formItemLayout} label="Tên">
                    <Input value={this.state.name} onChange={e => this.onChangeName(e.target.value)} />
                  </FormItem>
                  <FormItem {...formItemLayout} label="Role">
                    <Select value={this.state.role} onChange={this.onChangeRole}>
                      {
                        roles.map((item, indexItem) => {
                          return (
                            <Option key={indexItem} value={item.id}>{item.text}</Option>
                          )
                        })
                      }
                    </Select>
                  </FormItem>
                  {!id && <FormItem {...formItemLayout} label="Email">
                    {getFieldDecorator('email', {
                      rules: [{
                        type: 'email', message: 'The input is not valid E-mail!',
                      }, {
                        required: true, message: 'Please input your E-mail!',
                      }],
                      initialValue: this.state.email
                    })(
                      <Input onChange={e => this.onChangeEmail(e.target.value)} />
                    )}
                  </FormItem>}
                  {
                    id ? < FormItem {...formItemLayout} label="Điện thoại">
                      {
                        getFieldDecorator('phone', {
                          rules: [{
                            pattern: phoneValidate, message: 'Số điện thoại không đúng'
                          }],
                          initialValue: this.state.phone
                        })(
                          <Input onChange={e => this.onChangePhone(e.target.value)} />
                        )
                      }
                    </FormItem> : <FormItem {...formItemLayout} label="Mật khẩu">
                      <Input value={this.state.password} onChange={e => this.onChanePassword(e.target.value)} />
                    </FormItem>
                  }
                  {
                    id && <FormItem {...formItemLayout} label="Ảnh">
                      <Upload
                        accept="image/*"
                        beforeUpload={(file) => {
                          this.uploadImage(file)
                          return false
                        }}
                        showUploadList={false}
                        shape="square"
                      >
                        <Button>
                          <Icon type="upload" /> Tải ảnh
                        </Button>
                      </Upload>
                    </FormItem>
                  }
                  {
                    id && <FormItem {...tailFormItemLayout}>
                      <Avatar className="customer-avatar" src={this.state.avatar} />
                    </FormItem>
                  }
                  <FormItem {...tailFormItemLayout}>
                    <Button
                      type="primary"
                      onClick={id ? this.onUpdateUser : this.onCreateUser}
                    >
                      {id ? 'Cập nhập' : 'Tạo thành viên'}
                    </Button>
                    {
                      id && <Button
                        style={this.state.active ? {
                          backgroundColor: '#f5222d',
                          color: '#fff',
                          marginLeft: 10
                        } : { backgroundColor: '#52c41a', color: '#fff', marginLeft: 10 }}
                        onClick={this.changeStatus}
                      >{this.state.active ? 'Deactive' : 'Active'}</Button>
                    }
                    <Link to="/user">
                      <Button style={{ marginLeft: 10 }}>Quay lại</Button>
                    </Link>
                  </FormItem>
                </Form>
              </Col>
            </div>
          </Row>
        </Layout>
      </Layout>
    )
  }
}

const EditUser = Form.create()(FormUser)
export default connect(({ user, loading }) => ({ user, loading }))(EditUser)
