import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Link } from 'react-router-dom'
import { Layout, Form, Select, Input, Button, Row, Col } from 'antd'
import { RcBreadcrumbs } from '../../../components'

const FormItem = Form.Item
const Option = Select.Option
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  }
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 6,
    },
  }
}
const updateBreadcrumbs = [{ isLink: true, name: 'Role', path: '/role' }, {
  isLink: false,
  name: 'Chỉnh sửa',
  path: ''
}]
const createBreadcrumbs = [{ isLink: true, name: 'Role', path: '/role' }, {
  isLink: false,
  name: 'Tạo',
  path: ''
}]

class FormCreate extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func
  }

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      permissions: [],
      active: false
    }
  }

  componentWillMount() {
    this.getPermission()
    this.editOrCreate()
  }

  componentWillReceiveProps(nextProps) {
    this.resetState(nextProps)
  }

  onChangeName = (name) => {
    this.setState({ name })
  }

  onChangePermission = (permissions) => {
    this.setState({ permissions })
  }

  onUpdate = () => {
    const { name, permissions } = this.state
    this.props.dispatch({
      type: 'role/update',
      payload: {
        variables: {
          roleID: this.props.match.params.id,
          roleUpdate: { name, permissions }
        }
      }
    })
  }
  onCreate = () => {
    const { name, permissions } = this.state
    this.props.dispatch({
      type: 'role/create',
      payload: {
        variables: { roleCreate: { name, permissions } }
      }
    })
  }
  getPermission = () => {
    this.props.dispatch({
      type: 'role/allPermission',
      payload: {
        variables: {}
      }
    })
  }
  resetState = (props) => {
    const { role: { show } } = props
    this.setState({
      name: show.name,
      permissions: show.permissions,
      // active: show.active
    })
  }
  showRole = () => {
    this.props.dispatch({
      type: 'role/show',
      payload: {
        variables: { roleID: this.props.match.params.id }
      }
    })
  }
  editOrCreate = () => {
    if (this.props.match.params.id) {
      this.showRole()
    } else {
      this.setState({
        name: '',
        permissions: [],
        active: false
      })
    }
  }

  render() {
    const { id } = this.props.match.params
    const { role: { allPermission } } = this.props
    const permissions = []
    allPermission.map((item, index) => {
      permissions.push(<Option value={item} key={index}>{item}</Option>)
      return permissions
    })
    return (
      <Layout className="container">
        <RcBreadcrumbs
          name={id ? updateBreadcrumbs : createBreadcrumbs}
        />
        <Layout className="page-content">
          <Row>
            <div className="customer-box">
              <Col md={12} lg={12} sm={24} xs={24}>
                <Form>
                  <FormItem {...formItemLayout} label="Tên">
                    <Input value={this.state.name} onChange={e => this.onChangeName(e.target.value)} />
                  </FormItem>
                  <FormItem {...formItemLayout} label="Permission">
                    <Select mode="tags" onChange={this.onChangePermission} tokenSeparators={[',']} value={this.state.permissions}>
                      {
                        permissions
                      }
                    </Select>
                  </FormItem>
                  <FormItem {...tailFormItemLayout}>
                    <Button
                      type="primary"
                      onClick={id ? this.onUpdate : this.onCreate}
                    >
                      {id ? 'Cập nhập' : 'Tạo thành viên'}
                    </Button>
                    {
                      id && <Button
                        style={this.state.active ? {
                          backgroundColor: '#f5222d',
                          color: '#fff',
                          marginLeft: 10
                        } : { backgroundColor: '#52c41a', color: '#fff', marginLeft: 10 }}
                        onClick={this.changeStatus}
                      >{this.state.active ? 'Deactive' : 'Active'}</Button>
                    }
                    <Link to="/role">
                      <Button style={{ marginLeft: 10 }}>Quay lại</Button>
                    </Link>
                  </FormItem>
                </Form>
              </Col>
            </div>
          </Row>
        </Layout>
      </Layout>
    )
  }
}

const EditView = Form.create()(FormCreate)
export default connect(({ role, loading }) => ({ role, loading }))(EditView)
