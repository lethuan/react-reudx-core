import { requestGraphQL } from '../../utils/index'
import { ApiConst } from '../../configs/index'

export async function listUser(data = {}) {
  const api = ApiConst.user.listUser()
  return requestGraphQL({
    method: api.method,
    query: api.query,
    variables: data.variables
  })
}

export async function createUser(data = {}) {
  const api = ApiConst.user.create()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

export async function showUser(data = {}) {
  const api = ApiConst.user.show()
  return requestGraphQL({
    method: api.method,
    query: api.query,
    variables: data.variables
  })
}

export async function updateUser(data = {}) {
  const api = ApiConst.user.update()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

export async function changeStatusUser(data = {}) {
  const api = ApiConst.user.changeStatus()
  return requestGraphQL({
    method: api.method,
    query: api.mutation,
    variables: data.variables
  })
}

