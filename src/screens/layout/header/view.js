import React from 'react'
import PropTypes from 'prop-types'
import { Layout, Icon } from 'antd'
import { ProfileMenuView } from './profile-menu'
import './style.less'

const { Header } = Layout

class HeaderView extends React.Component {
  static propTypes = {
    onToggleSidebar: PropTypes.func,
    sidebarCollapsed: PropTypes.bool,
    user: PropTypes.object,
    logout: PropTypes.func
  }

  // Toggle sidebar collapse
  toggleSidebar = () => {
    this.props.onToggleSidebar()
  }

  // On select profile menu
  selectProfileItem = (key) => {
    if (key === 'logout') {
      this.props.logout()
    }
  }

  // // On select notification menu
  // selectNotificationItem = (key) => {
  //   console.log('Select notification menu with key:', key)
  // }

  render() {
    const { sidebarCollapsed, user } = this.props
    return (
      <Header className="app-header">
        <Icon
          className="icon-trigger"
          type={sidebarCollapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.toggleSidebar}
        />

        <ProfileMenuView
          onSelectMenuItem={this.selectProfileItem}
          user={user}
        />
      </Header>
    )
  }
}

export default HeaderView
