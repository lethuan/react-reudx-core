import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Row, Table, Icon } from 'antd'

const colums = (context) => {
  const { current, pageSize } = context.props
  return [{
    title: 'STT',
    dataIndex: '',
    className: 'hidden-break-small',
    render: (value, row, index) => {
      return (<span>{(current * pageSize) + (index + 1)}</span>)
    }
  }, {
    title: 'Thành viên',
    dataIndex: 'name',
    render: (value, row) => {
      return (
        <span className="registered-user" onClick={() => context.props.onViewUser(row._id)}>{value}</span>
      )
    }
  }, {
    title: 'Email',
    dataIndex: 'email',
    render: (value) => {
      return (
        <span>{value}</span>
      )
    }
  }, {
    title: 'Role',
    dataIndex: 'role',
    render: (value) => {
      return (
        <span>{value}</span>
      )
    }
  }, {
    title: 'SĐT',
    dataIndex: 'phone',
    render: (value) => {
      return (
        <span>{value}</span>
      )
    }
  }, {
    title: 'Sửa',
    dataIndex: '_id',
    render: (value) => {
      return (
        <Link to={`user/${value}/edit`}>
          <Icon type="edit" />
        </Link>
      )
    }
  }]
}

class TableView extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    pageSize: PropTypes.number,
    total: PropTypes.number,
    current: PropTypes.number,
    onChange: PropTypes.func
  }

  render() {
    const { pageSize, total, current, data, onChange } = this.props
    return (
      <Row className="background-white">
        <Table
          className="app-table"
          defaultCurrent={0}
          columns={colums(this)}
          rowKey="_id"
          dataSource={data}
          pagination={{ pageSize, total, current: current + 1 }}
          onChange={onChange}
        />
      </Row>
    )
  }
}

export default TableView
